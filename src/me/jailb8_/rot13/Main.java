package me.jailb8_.rot13;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	Server s = this.getServer();
	public void onEnable(){
		PluginManager pm = s.getPluginManager();
		pm.registerEvents(this, this);
		this.saveDefaultConfig();
		getConfig().options().copyDefaults(true);
		saveConfig();

		
		if(getConfig().getInt("ROT") == 13 || getConfig().getInt("ROT") == 47){
			s.getLogger().log(Level.INFO, "Loaded Successfully. Will use ROT" + getConfig().getInt("ROT"));
		}else{
			s.getLogger().log(Level.SEVERE, "Invalid ROT format provided. Plugin will disable. Edit in config.");
			pm.disablePlugin(this);
		}
	}
	public void onDisable(){
		
	}
	
		private Set<UUID> busy = new HashSet<>();
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		UUID id = p.getUniqueId();
		String message = e.getMessage();
		if(getConfig().getBoolean("ROT-BY-DEFAULT")){
			
			if(busy.contains(id)){
			busy.remove(id);
			return;
		}
		
		busy.add(id);
		if(getConfig().getInt("ROT") == 13){
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < message.length(); i++){
				char c = message.charAt(i);
				if       (c >= 'a' && c <= 'm') c += 13;
				else if  (c >= 'A' && c <= 'M') c += 13;
				else if  (c >= 'n' && c <= 'z') c -= 13;
				else if  (c >= 'N' && c <= 'Z') c -= 13;
				sb.append(c);
			}
			p.chat(sb.toString());
			s.getLogger().info("[ROT13] " + sb.toString() + " : " + message);;
			e.setCancelled(true);
		}else if(getConfig().getInt("ROT") == 47){
			int length = message.length();
			StringBuilder result = new StringBuilder();
			for (int i = 0; i < length; i++){
				char c = message.charAt(i);
				if (c != ' '){
					c += 47;
					
					if (c > '~')
						c -= 94;
				}
				result.append(c);
			}
			p.chat(result.toString());
			s.getLogger().info("[ROT47] " + result.toString() + " : " + message);
			e.setCancelled(true);
		}else{
			s.getLogger().severe("ROT IS NOT DEFINED CORRECTLY FOR CHAT. PLEASE USE 13 OR 47.");
		}
	}
}}
